import pandas as pd 
import datetime
import pandas_datareader as pdr 
import matplotlib.pyplot as plt 
from matplotlib import style

def get_requirements():
    print("Data Analysis 1")
    
    print("\nProgram Requirements:")
    print("1. Run demo.py.\n"
    + "2. If errors, more than likely missing installations.\n"
    + "3. Test Python Package Installer: pip freeze.\n"
    + "4. Research how to do the folliwing installations:\n"
        + "\ta. pandas (only if missing)\n"
        + "\tb. pandas-datareader (only if missing)\n"
        + "\tc. matplotlib (only if missing)\n"
    + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls atleast two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. data_analysis_1(): displays the following data.\n")

def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.today()

    df = pdr.DataReader("XOM","yahoo",start,end)

    print("\nPrint number of records: ")
    print(len(df.index))

    print("\nPrint columns:")
    print(df.columns)
    
    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines: ")
    print(df.head(5))

    print("\nPrint last five lines: ")
    print(df.tail(5))

    print("\nPrint first 2 lines: ")
    print(df.head(2))

    print("\nPrint last 2 lines: ")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()

    
    

