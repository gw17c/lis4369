> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Project 1 Requirements:

*Four Parts:*

1. Requirements:
    a. Code and run demo.py. (Note: *be sure* necessary packages are installed!)
    b. Then use it to backward-engineer the screenshots below it.
    c. Installing Python packages pip helper videos
2. Be sure to test your program using both IDLE and Visual Studio Code
3. Questions
4. Bitbucket repo links:
    a. this assignment 

#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshot as per examples below

#### Assignment Screenshots:

*Screenshot of p1_data_analysis_1 application running (Visual Studio Code):*

![App Running in Visual Studio Screenshot](img/vs1.png)
![App Running in Visual Studio Screenshot](img/vs2.png)
![App Running in Visual Studio Screenshot](img/vs3.png)

*Screenshot of p1_data_analysis_1 application running (IDLE):*

![App Running in IDLE Screenshot](img/idle1.png)
![App Running in IDLE Screenshot](img/idle2.png)
![App Running in IDLE Screenshot](img/idle3.png)

*Screenshot of graph:*

![Graph after running App](img/graph.png)


#### Links:

*a) Assignment:*
[P1 Link](https://bitbucket.org/gw17c/lis4369/src/master/p1/)


