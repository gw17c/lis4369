> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4369 - Extensible Enterprise Solutions

## Gal Wolf

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/gw17c/lis4369/src/master/a1/README.md)
    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create a1_tip_calculator application
    * Provide screenshots of installations
    * Create Bitbuckket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/gw17c/lis4369/src/master/a2/README.md)
    * Create a2_payroll_calculator application
    * Provide screenshots running in IDLE and Visual Studio Code
3. [A3 README.md](https://bitbucket.org/gw17c/lis4369/src/master/a3/README.md)
    * Create a3_painting_estimator application
    * Provide screenshots running in IDLE and Visual Studio Code
4. [A4 README.md](https://bitbucket.org/gw17c/lis4369/src/master/a4/README.md)
    * Code and run demo.py
    * Create a4_data_analysis_2 by backward-engineering the screenshots below
    * Provide screenshots running in IDLE and Visual Studio Code
5. [A5 README.md](https://bitbucket.org/gw17c/lis4369/src/master/a5/README.md)
    * Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
    * Code and run lis4369_a5.R
    * Be sure to include at least two plots in your README.md file.
    * Be sure to test your program using RStudio
6. [P1 README.md](https://bitbucket.org/gw17c/lis4369/src/master/p1/README.md)
    * Install Python packages pip
    * Code and run demo.py
    * Create p1_data_analysis_1 by backward-engineering the screenshots below
    * Provide screenshots running in IDLE and Visual Studio Code
7. [P2 README.md](https://bitbucket.org/gw17c/lis4369/src/master/p2/README.md)
    * Backward-engineer the lis4369_p2_requirements.txt file
    * Be sure to include at least two plots in your README.md file
    * Be sure to test your program using RStudio
