> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:
    a) this assignment and
    b) the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create an empty git repo
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repo
5. git push - update remote refs along w/ associated objects
6. git pull - fetch from and integrate with another repo
7. git config - get and set repo or global options

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator application running (IDLE):*

![App Running in IDLE Screenshot](img/idle.png)

*Screenshot of a1_tip_calculator application running (Visual Studio Code):*

![App Running in Visual Studio Screenshot](img/vsc.png)


#### Links:

*a) Assignment:*
[A1 Link](https://bitbucket.org/gw17c/lis4369/src/master/a1/)

*b) BitbucketStationLocations tutorial:*
[Tutorial Repo Link](https://bitbucket.org/gw17c/bitbucketstationlocations/src/master/)

