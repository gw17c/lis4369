# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Assignment 5 Requirements:

*Four Parts:*

1. Requirements:
    1. Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
    2. Code and run lis4369_a5.R
    3. Be sure to include at least two plots in your README.md file.
2. Be sure to test your program using RStudio
3. Questions
4. Bitbucket repo links:
    1. this assignment 

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots of output from code below.

#### Assignment Screenshots:

*Screenshot of lis4369_a5.R application running (RStudio):*

![App Running in RStudio](image/r1.jpg) ![App Running in RStudio](image/r2.jpg)
![App Running in RStudio](image/r3.jpg) ![App Running in RStudio](image/r4.jpg)
![App Running in RStudio](image/r5.jpg) ![App Running in RStudio](image/r6.jpg)
![App Running in RStudio](image/r7.jpg) ![App Running in RStudio](image/r8.jpg)
![App Running in RStudio](image/r9.jpg) ![App Running in RStudio](image/r10.jpg)
![App Running in RStudio](image/r11.jpg) ![App Running in RStudio](image/r12.jpg)
![App Running in RStudio](image/r13.jpg) ![App Running in RStudio](image/r14.jpg)

*Screenshot of plots after running application:*

![Plots after running App](image/plot1.jpg) ![Plots after running App](image/plot2.jpg)
![Plots after running App](image/plot3.jpg) ![Plots after running App](image/plot4.jpg)
![Plots after running App](image/plot5.jpg)

#### Link:

*a) Assignment:*
[A5 Link](https://bitbucket.org/gw17c/lis4369/src/master/a5/)

### R, Setup, and Tutorial Requirements:

*Three Parts:*

1. What is R? (and, download): https://www.youtube.com/watch?v=X67No4239Ys
2. What is RStudio? (and, download): https://www.youtube.com/watch?v=e_qxDl9xEV8
3. Carefully go through the entire following tutorial (up to and including p. 32):
LEARN TO USE R Your hands-on guide: http://core0.staticworld.net/assets/2015/02/17/r4beginners.pdf
Note: When the above tutorial is successfully completed, the below deliverables will be fulfilled

*Deliverables:*

1. R Commands: save a file of all the R commands included in the tutorial.
2. R Console: save a screenshot of some of the R commands executed above (below requirement).
3. Graphs: save at least 5 separate image files displaying graph plots created from the tutorial.
4. RStudio: save one screenshot (similar to the one below), displaying the following 4 windows:
    1. R source code (top-left corner)
    2. Console (bottom-left corner)
    3. Environment (or History), (top-right corner)
    4. Plots (bottom-right corner)

#### Tutorial Screenshots:

*Screenshot of R Console running some of the commands:*

![Commands run in R Console](tutorial/rconsole.jpg)

*Screenshot of Graph Plots created from the tutorial:*

![Plots from tutorial](tutorial/plot1.jpg) ![Plots from tutorial](tutorial/plot2.jpg)
![Plots from tutorial](tutorial/plot3.jpg) ![Plots from tutorial](tutorial/plot4.jpg)
![Plots from tutorial](tutorial/plot5.jpg) ![Plots from tutorial](tutorial/plot6.jpg)

*Screenshot of RStudio displaying the 4 windows:*

![RStudio window](tutorial/rstudio.jpg)

#### File:

*R Commands:*
[File](https://bitbucket.org/gw17c/lis4369/src/master/a5/tutorial.Rhistory)