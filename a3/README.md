> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Assignment 3 Requirements:

*Four Parts:*

1. Backward-engineer (using Python) the following screenshots. The program should be organized with two modules:
    a) functions.py contains the following functions:
        i) get_requirements()
        ii) estimate_painting_cost()
        ii) print_painting_estimate()
        iii) print_painting_percentage()
    b) main.py
2. Test your program using both IDLE and Visual Studio Code
3. Questions
4. Bitbucket repo links:
    a) this assignment 

#### README.md file should include the following items:

* Assignmnet requirements, as per A1
* Screenshot as per examples below

#### Assignment Screenshots:

*Screenshot of a3_painting_estimator application running (IDLE):*

![App Running in IDLE Screenshot](img/idle.png)

*Screenshot of a3_painting_estimator application running (Visual Studio Code):*

![App Running in Visual Studio Screenshot](img/visual.png)

#### Links:

*a) Assignment:*
[A3 Link](https://bitbucket.org/gw17c/lis4369/src/master/a3/)


