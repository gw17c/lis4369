import functions as f

def main():
    f.get_requirements()
    f.estimate_painting_cost()

    option = str(input("\nEstimate another paint job? (y/n): "))
    if (option == "y"):
        while (option == "y"):
            f.estimate_painting_cost()
            option = str(input("\nEstimate another paint job? (y/n): "))
            if (option != "y"):
                print("Thank you for using our Painting Estimator!")
                print("Please see our web site: https://linkedin.com/in/gal-wolf")
            else:
                continue
    else:
        print("Thank you for using our Painting Estimator!")
        print("Please see our web site: https://linkedin.com/in/gal-wolf")
        

if __name__ == "__main__":
    main()