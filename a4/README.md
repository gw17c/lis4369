> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Assignment 4 Requirements:

*Four Parts:*

1. Requirements:
    1. Code and run demo.py. (Note: *be sure* necessary packages are installed!)
    2. Then use it to backward-engineer the screenshots below it.
    3. When displaying the required graph (see code below), answer the following question: Why is the graph line split?
2. Be sure to test your program using both IDLE and Visual Studio Code
3. Questions
4. Bitbucket repo links:
    a. this assignment 

#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshot as per examples below

#### Assignment Screenshots:

*Screenshot of a4_data_analysis_2 application running (Visual Studio Code):*

![App Running in Visual Studio Screenshot](img/vs1.jpg) ![App Running in Visual Studio Screenshot](img/vs2.jpg)
![App Running in Visual Studio Screenshot](img/vs3.jpg) ![App Running in Visual Studio Screenshot](img/vs4.jpg)
![App Running in Visual Studio Screenshot](img/vs5.jpg) ![App Running in Visual Studio Screenshot](img/vs6.jpg)
![App Running in Visual Studio Screenshot](img/vs7.jpg) ![App Running in Visual Studio Screenshot](img/vs8.jpg)
![App Running in Visual Studio Screenshot](img/vs9.jpg) ![App Running in Visual Studio Screenshot](img/vs10.jpg)
![App Running in Visual Studio Screenshot](img/vs11.jpg) ![App Running in Visual Studio Screenshot](img/vs12.jpg)


*Screenshot of a4_data_analysis_2 application running (IDLE):*

![App Running in IDLE Screenshot|1148x1360,25%](img/idle1.jpg) ![App Running in IDLE Screenshot|1148x1360,25%](img/idle2.jpg)
![App Running in IDLE Screenshot|1148x1360,25%](img/idle3.jpg) ![App Running in IDLE Screenshot|1148x1360,25%](img/idle4.jpg)
![App Running in IDLE Screenshot|1148x1360,25%](img/idle5.jpg) ![App Running in IDLE Screenshot|1148x1360,25%](img/idle6.jpg)
![App Running in IDLE Screenshot|1148x1360,25%](img/idle7.jpg) ![App Running in IDLE Screenshot|1148x1360,25%](img/idle8.jpg)
![App Running in IDLE Screenshot|1148x1360,25%](img/idle9.jpg) ![App Running in IDLE Screenshot|1148x1360,25%](img/idle10.jpg)
![App Running in IDLE Screenshot|1148x1360,25%](img/idle11.jpg) ![App Running in IDLE Screenshot|1148x1360,25%](img/idle12.jpg)

*Screenshot of graph:*

![Graph after running App](img/graph.png)


#### Links:

*a) Assignment:*
[A4 Link](https://bitbucket.org/gw17c/lis4369/src/master/a4/)


