def get_requirements():
    print("Payroll Calculator")
    print("\nProgram Requirements:")
    print("1. Must use float data type for user input.\n"
    + "2. Overtime rate: 1.5 times hourly rate (hoursover 40).\n"
    + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
    + "4. Must format currency with dollar sign, and round to two decimal places\n"
    + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls at least two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. calculate_payroll(): calculates an individual one-week paycheck.")

def calculate_payroll():

    print("\nInput:")
    worked = float(input("Enter hours worked: "))
    holi_hours = float(input("Enter holiday hours: "))
    hourly = float(input("Enter hourly pay rate: "))

    if (worked > 40.0):
        base = round(40.0 * hourly, 2)
        overtime = round(((worked - 40.0) * (1.5 * hourly)), 2)
        holiday = round((holi_hours * (2.0 * hourly)), 2)
        gross = base + overtime + holiday
        print_pay(base, overtime, holiday, gross)
    else: 
        base = round(worked * hourly, 2)
        overtime = round((0 * (1.5 * 10)), 2)
        holiday = round((holi_hours * (2.0 * hourly)), 2)
        gross = base + overtime + holiday
        print_pay(base, overtime, holiday, gross)


def print_pay(base, overtime, holiday, gross):
    print("\nOutput:")
    print("{0:17} ${1:>5.2f}".format("Base:", base))
    print("{0:17} ${1:>5.2f}".format("Overtime:", overtime))
    print("{0:17} ${1:>5.2f}".format("Holiday:", holiday))
    print("{0:17} ${1:>5.2f}".format("Gross:", gross))
    print("\n")