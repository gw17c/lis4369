> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Assignment 2 Requirements:

*Four Parts:*

1. Backward-engineer (using Python) the following screenshots. The program should be organized with two modules
    a) functions.py contains the following functions:
        i) get_requirements()
        ii) calculate_payroll()
        ii) print_pay()
    b) main.py
2. Test your program using both IDLE and Visual Studio Code
3. Questions
4. Bitbucket repo links:
    a) this assignment 

#### README.md file should include the following items:

* Assignmnet requirements, as per A1
* Screenshots as per examples below

#### Assignment Screenshots:

*Screenshot of a2_payroll_calculator application running (IDLE) with no overtime:*

![App Running in IDLE Screenshot](img/idle_no_ot.png)

*Screenshot of a2_payroll_calculator application running (IDLE) with overtime:*

![App Running in IDLE Screenshot](img/idle_ot.png)

*Screenshot of a2_payroll_calculator application running (Visual Studio Code) with no overtime:*

![App Running in Visual Studio Screenshot](img/vs_no_ot.png)

*Screenshot of a2_payroll_calculator application running (Visual Studio Code) with overtime:*

![App Running in Visual Studio Screenshot](img/vs_ot.png)


#### Links:

*a) Assignment:*
[A2 Link](https://bitbucket.org/gw17c/lis4369/src/master/a2/)


