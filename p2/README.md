# LIS4369 Extensible Enterprise Solutions

## Gal Wolf

### Project 2 Requirements:

*Four Parts:*

1. Requirements:
    1. Use assignment 5 screenshots and references above for the following requirements:
    2. Backward-engineer the lis4369_p2_requirements.txt file
    3. Be sure to include at least two plots in your README.md file.
2. Be sure to test your program using RStudio
3. Questions
4. Bitbucket repo links:
    1. this assignment 

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots of output from code below.

#### Assignment Screenshots:

*Screenshot of lis4369_p2.R application:*

![lis4369_p2.R](img/r1.jpg) ![lis4369_p2.R](img/r2.jpg)


*Screenshot of plots after running application:*

![Plots after running App](img/plot.jpg) ![Plots after running App](img/qplot.jpg)


#### Links:

*a) Assignment:*
[P2 Link](https://bitbucket.org/gw17c/lis4369/src/master/p2/)

